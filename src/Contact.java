import java.util.ArrayList;

/**
 * Class representing a contact in Cloud Computing HW4
 * @author Joseph Malandruccolo
 *
 */
public class Contact {
	
	//=====================================================================
	//	=>	PROPERTIES
	//=====================================================================
	private String firstName;
	private String lastName;
	private ArrayList<Telephone> tele;
	private ArrayList<Email> email;
	private String address;
	private ArrayList<String> tags;
	private String birthday;
	public String filename;
	
	//=====================================================================
	//	=>	CONSTRUCTOR
	//=====================================================================
	public Contact(String firstName) {
		this.firstName = firstName;
		this.email = new ArrayList<Contact.Email>();
		this.tele = new ArrayList<Contact.Telephone>();
		this.tags = new ArrayList<String>();
		
	}
	
	//=====================================================================
	//	=>	GETTERS AND SETTERS
	//=====================================================================
	
	
	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTele() {
		StringBuilder sb = new StringBuilder();
		for (Telephone e : this.tele) {
			sb.append(e.label);
			sb.append(" : ");
			sb.append(e.number);
			sb.append(" | ");
		}
		
		return sb.toString();
		
	}

	public void addTele(String number, String tag) {
		this.tele.add(new Telephone(number, tag));
	}

	public String getEmail() {
		StringBuilder sb = new StringBuilder();
		for (Email e : this.email) {
			sb.append(e.label);
			sb.append(" : ");
			sb.append(e.address);
			sb.append(" | ");
		}
		
		return sb.toString();
		
	}

	public void addEmail(String address, String label) {
		this.email.add(new Email(address, label));
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTags() {
		StringBuilder sb = new StringBuilder();
		for (String tag : this.tags) {
			sb.append(tag);
			sb.append(" ");
		}
		
		return sb.toString();
		
	}

	public void addTag(String tag) {
		this.tags.add(tag);
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
	
	
	//=====================================================================
	//	=>	ANONYMOUS INNER CLASS
	//=====================================================================
	public class Telephone {
		
		//	PROPERTIES
		String number;
		String label;
		
		
		//	CONSTRUCTOR
		public Telephone(String number, String label) {
			this.number = number;
			this.label = label;
		}
		
		//	TOSTRING
		public String toString() { return this.label + ": " + this.number; }
		
	}// end Telphone
	
	
	public class Email {
		
		//	PROPERTIES
		String address;
		String label;
		
		
		//	CONSTRUCTOR
		Email(String address, String label) {
			this.address = address;
			this.label = label;
		}
		
		//	TOSTRING
		public String toString() { return this.label + ": " + this.address; }
		
	}// end Email
	
	
	
}//end Contact
