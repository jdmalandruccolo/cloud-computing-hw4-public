
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;


import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.CreateDomainRequest;
import com.amazonaws.services.simpledb.model.DeleteAttributesRequest;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.PutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.ListTopicsResult;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.Topic;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

/**
 * This class serves as a console-based app to manage a contacts database
 * stored in Amazon's SimpleDB.
 * @author Joseph Malandruccolo
 *
 */
public class SimpleDBConsoleApp {
	
	
	//=====================================================================
	//	=>	CONSTANTS
	//=====================================================================
	public static final String DB_NAME = "malandruccolodb";
	public static final String CREATE_MESSAGE = "Contact created";
	public static final String EDIT_MESSAGE = "Contact modified";
	
	private static final String TOPIC_ARN = "arn:aws:sns:us-east-1:364316631894:51083-updated";
	
	//=====================================================================
	//	=>	AWS OBJECTS
	//=====================================================================
	static AmazonEC2      ec2;
    static AmazonS3       s3;
    static AmazonSimpleDB sdb;
    static AmazonSNS	  sns;
    
    
    //=====================================================================
	//	=>	INITIALIZATION
	//=====================================================================
    /**
     * The only information needed to create a client are security credentials
     * consisting of the AWS Access Key ID and Secret Access Key. All other
     * configuration, such as the service endpoints, are performed
     * automatically. Client parameters, such as proxies, can be specified in an
     * optional ClientConfiguration object when constructing a client.
     *
     * @see com.amazonaws.auth.BasicAWSCredentials
     * @see com.amazonaws.auth.PropertiesCredentials
     * @see com.amazonaws.ClientConfiguration
     */
    private static void init() throws Exception {
    	/*
		 * This credentials provider implementation loads your AWS credentials
		 * from a properties file at the root of your classpath.
		 */
        //AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider();

        //ec2 = new AmazonEC2Client(credentialsProvider);
        s3  = new AmazonS3Client();
        sdb = new AmazonSimpleDBClient();
        sns = new AmazonSNSClient();
        
        
    }
    
    //=====================================================================
	//	=>	MAIN METHOD
	//=====================================================================
	public static void main(String[] args) {
		
		try { init(); }
		catch (Exception e) {
			System.out.println("Credentials file not found\nPlease create the property file at the root of your classpath");
		}
		
		
		
		//	UI
		System.out.println("======================================================================");
		System.out.println("\tWelcome to the SimpleDB/S3 Contact Management System");
		System.out.println("======================================================================");
		
		
		//	Create new sdb and s3 instances and SNS
		Region usEast1 = Region.getRegion(Regions.US_EAST_1);
		s3.setRegion(usEast1);
		sdb.setRegion(usEast1);
		
		sdb.createDomain(new CreateDomainRequest(DB_NAME));
		
		//	get the SNS setvices
		sns.setRegion(usEast1);
		
		
		//	User defined actions
		while (true) {
			
			
			//	UI
			System.out.println("Navigate this AWS command line app by selecting a number to trigger an action:\n");
			System.out.println("1 - List all contacts");
			System.out.println("2 - Show details for a contact");
			System.out.println("3 - Edit details for a contact");
			System.out.println("4 - Create a new contact");
			System.out.println("5 - Search for contact by attribute");
			System.out.println("9 - EXIT");
			
			
			//	GET USER INPUT
			Scanner s = new Scanner(System.in);
			String rawInput = s.nextLine();
			int parsedInput = 0;
			try { parsedInput = Integer.parseInt(rawInput); }
			catch (NumberFormatException e) { System.out.println(rawInput + " is not a valid number\nPlease enter a valid number\n");}
			
			
			//	HANDLE USER INPUTS
			switch (parsedInput) {
			case 1:
				listContacts();
				break;
				
			case 2:
				showDetailsForContact();
				break;
				
			case 3:
				editDetailsForContact();
				break;
				
			case 4:
				createNewContact(CREATE_MESSAGE);
				break;
				
			case 5:
				searchForContactByAttribute();
				break;
				
			case 9:
				System.exit(1);
				break;

			default:
				//	DO NOTHING, CONTINUE IN WHILE LOOP
				break;
			}
			
		}
		
		
		
	}

	
	

	

	//=====================================================================
	//	=>	CONSOLE TRIGGERED METHODS
	//=====================================================================
	private static void listContacts() {
		
		String query = "select * from `" + DB_NAME + "`";
		SelectRequest sr = new SelectRequest(query);
		for (Item i : sdb.select(sr).getItems()) System.out.println(i.getName());
		
	}
	
	
	private static void editDetailsForContact() {
		
		System.out.println("Choose a contact below to edit details:");
		String query = "select * from `" + DB_NAME + "`";
		SelectRequest sr = new SelectRequest(query);
		List<Item> items = sdb.select(sr).getItems();
		HashMap<String, Item> itemMap = new HashMap<String, Item>();
		for (Item i : items) {
			System.out.println(i.getName());
			itemMap.put(i.getName(), i);
		}
		
		System.out.println("Type the name to edit details:");
		Scanner s = new Scanner(System.in);
		String contact = s.nextLine();
		
		Item i = itemMap.get(contact);
		System.out.println("Attributes for: " + i.getName());
		List<com.amazonaws.services.simpledb.model.Attribute> attributes = i.getAttributes();
		for (com.amazonaws.services.simpledb.model.Attribute a : attributes) {
			System.out.println(a.getName() + " : " + a.getValue());
		}
		
		sdb.deleteAttributes(new DeleteAttributesRequest(DB_NAME, i.getName()));
		
		System.out.println("Type updated contact info:");
		createNewContact(EDIT_MESSAGE);
		
	}

	
	private static void showDetailsForContact() {
		
		System.out.println("Choose a contact below to view details:");
		String query = "select * from `" + DB_NAME + "`";
		SelectRequest sr = new SelectRequest(query);
		List<Item> items = sdb.select(sr).getItems();
		HashMap<String, Item> itemMap = new HashMap<String, Item>();
		for (Item i : items) {
			System.out.println(i.getName());
			itemMap.put(i.getName(), i);
		}
		
		System.out.println("Type the name to view details:");
		Scanner s = new Scanner(System.in);
		String contact = s.nextLine();
		
		Item i = itemMap.get(contact);
		System.out.println("Attributes for: " + i.getName());
		List<com.amazonaws.services.simpledb.model.Attribute> attributes = i.getAttributes();
		for (com.amazonaws.services.simpledb.model.Attribute a : attributes) {
			System.out.println(a.getName() + " : " + a.getValue());
		}
	}
	
	private static void createNewContact(String message) {
		
		//	first name
		Scanner s = new Scanner(System.in);
		System.out.println("Enter a first name:");
		String firstName = s.nextLine();
		Contact c = new Contact(firstName);
		
		//	last name
		System.out.println("Enter a last name (press 0 to skip)");
		String lastName = s.nextLine();
		if (!lastName.equals("0")) c.setLastName(lastName);
		
		//	phone number
		System.out.println("Enter phone numbers (yes 'Y' or no 'N')");
		String addNumbers = s.nextLine();
		if (addNumbers.equalsIgnoreCase("Y")) promptForContactMethod(c, Contact.Telephone.class);
			
		//	email
		System.out.println("Enter email addresses (yes 'Y' or no 'N')");
		String addEmails = s.nextLine();
		if (addEmails.equalsIgnoreCase("Y")) promptForContactMethod(c, Contact.Email.class); 
		
		//	address
		System.out.println("Enter an address (press 0 to skip)");
		String address = s.nextLine();
		if (!address.equals("0")) c.setAddress(address);
		
		//	tags
		System.out.println("Enter additional tags (yes 'Y' or no 'N')");
		String tags = s.nextLine();
		if (tags.equalsIgnoreCase("Y")) promptForContactMethod(c, String.class);
		
		//	birthday
		System.out.println("Enter a birthday (press 0 to skip)");
		String bday = s.nextLine();
		if (!bday.equals("0")) c.setBirthday(bday);
		
		writeContactToSimpleDB(c);
		try {
			
			writeContactToS3(c);
			publishNotificationWithMessage(message, c);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Write to s3 failed");;
		}
		
	}
	
	
	private static void searchForContactByAttribute() {
		
		
		System.out.println("Search for contact by attribute:");
		System.out.println("1 - First Name");
    	System.out.println("2 - Last Name");
    	System.out.println("3 - Birthday");
    	System.out.println("4 - Email");
    	System.out.println("5 - Tags");
    	Scanner s = new Scanner(System.in);
    	String rawInput = s.nextLine();
    	int parsedInput = 0;
    	try { parsedInput = Integer.parseInt(rawInput); }
		catch (NumberFormatException e) { System.out.println(rawInput + " is not a valid number\nReturning all results\n");}
    	
    	String query = "select * from `" + DB_NAME + "`";
    	switch (parsedInput) {
		case 1:
			System.out.println("Enter first name to search");
			String firstName = s.nextLine();
			query = "select * from `" + DB_NAME + "`where firstName like '%" + firstName + "%'";
			break;
		case 2:
			System.out.println("Enter last name to search");
			String lastName = s.nextLine();
			query = "select * from `" + DB_NAME + "`where lastName like '%" + lastName + "%'";
			break;
			
		case 3:
			System.out.println("Enter birthday to search");
			String birthday = s.nextLine();
			query = "select * from `" + DB_NAME + "`where birthday like '%" + birthday + "%'";
			break;
			
		case 4:
			System.out.println("Enter an email to search");
			String email = s.nextLine();
			query = "select * from `" + DB_NAME + "`where email like '%" + email + "%'";
			break;
			
		case 5:
			System.out.println("Enter tag to search");
			String tag = s.nextLine();
			query = "select * from `" + DB_NAME + "`where tags like '%" + tag + "%'";
			break;

		default:
			break;
		}
    	
    	System.out.println("Search results:");
    	SelectRequest sr = new SelectRequest(query);
    	List<Item> items = sdb.select(sr).getItems();
    	if (items.size() > 0) {
    		for (Item i : items) System.out.println(i.getName());
		}
    	else {
    		System.out.println("No results found");
    	}
		
		
	}
	
	
	//=====================================================================
	//	=>	PRIVATE HELPERS
	//=====================================================================
	private static void promptForContactMethod(Contact c, @SuppressWarnings("rawtypes") Class innerClass) {
		
		if (innerClass == Contact.Telephone.class) {
			//read telephone
			while (true) {
				System.out.println("Enter a phone number (type 'C' to continue)");
				Scanner s = new Scanner(System.in);
				String number = s.nextLine();
				if (number.equalsIgnoreCase("C")) break;
				
				System.out.println("Enter a label for phone number");
				String tag = s.nextLine();
				c.addTele(number, tag);
				
			}
		}
		else if (innerClass == Contact.Email.class) {
			//read email
			while (true) {
				System.out.println("Enter an email (type 'C' to continue)");
				Scanner s = new Scanner(System.in);
				System.out.println("Enter a label for the email");
				String number = s.nextLine();
				if (number.equalsIgnoreCase("C")) break;
				
				System.out.println("Enter a label for email");
				String tag = s.nextLine();
				c.addTele(number, tag);
			}
		}
		else if (innerClass == String.class) {
			//read tags
			while (true) {
				System.out.println("Enter an tag (type 'C' to continue)");
				Scanner s = new Scanner(System.in);
				String number = s.nextLine();
				if (number.equalsIgnoreCase("C")) break;
				c.addTag(number);
			}
		}
		else throw new IllegalArgumentException("Contact type not recognized");
		
	}
	
	
	private static void writeContactToSimpleDB(Contact c) {
		
		
		ArrayList<ReplaceableAttribute> attributes = new ArrayList<ReplaceableAttribute>();
		attributes.add(new ReplaceableAttribute("firstName", c.getFirstName(), true));
		if (c.getLastName() != null) attributes.add(new ReplaceableAttribute("lastName", c.getLastName(), true));
		if (c.getTele() != null) attributes.add(new ReplaceableAttribute("phoneNumbers", c.getTele(), true));
		if (c.getEmail() != null) attributes.add(new ReplaceableAttribute("email", c.getEmail(), true));
		if (c.getAddress() != null) attributes.add(new ReplaceableAttribute("address", c.getAddress(), true));
		if (c.getTags() != null) attributes.add(new ReplaceableAttribute("tags", c.getTags(), true));
		if (c.getBirthday() != null) attributes.add(new ReplaceableAttribute("birthday", c.getBirthday(), true));
		
		
		sdb.putAttributes(new PutAttributesRequest(DB_NAME, c.getFirstName()+c.getLastName(), attributes));
		
		
	}
	
	
	private static void writeContactToS3(Contact c) throws IOException {
		
		//	UI Choose a bucket
		System.out.println("Contact will be added to a bucket");
		System.out.println("Available buckets:");
		for (Bucket bucket : s3.listBuckets()) System.out.println(bucket.getName());
		
		System.out.println("Enter the name of the bucket you would like to create the object in");
        Scanner s = new Scanner(System.in);
        String bucketName = s.nextLine();
        
        //      create the html file
        String fileName = c.getFirstName() + c.getLastName() + System.currentTimeMillis();
        c.filename = fileName.toLowerCase();
        File f = File.createTempFile(fileName.toLowerCase(), "html");
        BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        
        StringBuffer sb = new StringBuffer();
        sb.append("<html><head><title>Contact Page</title></head><body><table><tr><th>First Name</th>");
        if (c.getLastName() != null) sb.append("<th>Last Name</th>");
        if (c.getTele() != null) sb.append("<th>Phone Number</th>");
        if (c.getEmail() != null) sb.append("<th>Email</th>");
        if (c.getAddress() != null) sb.append("<th>Address</th>");
        if (c.getBirthday() != null) sb.append("<th>Birthday</th>");
        if (c.getTags() != null) sb.append("<th>Tags</th>");
        sb.append("</tr>");
        sb.append("<tr><td>"+c.getFirstName()+"</td>");
        if (c.getLastName() != null) sb.append("<td>"+c.getLastName()+"</td>");
        if (c.getTele() != null) sb.append("<td>"+c.getTele()+"</td>");
        if (c.getEmail() != null) sb.append("<td>"+c.getEmail()+"</td>");
        if (c.getAddress() != null) sb.append("<td>"+c.getAddress()+"</td>");
        if (c.getBirthday() != null) sb.append("<td>"+c.getBirthday()+"</td>");
        if (c.getTags() != null) sb.append("<td>"+c.getTags()+"</td>");
        sb.append("</tr></body></html>");
        
        bw.write(sb.toString());
        bw.close();
        
        
        //lowercase to comply with DNS
        String objectId = fileName.toLowerCase() + ".html";
        PutObjectRequest por = new PutObjectRequest(bucketName, objectId, f);
        por.setCannedAcl(CannedAccessControlList.PublicRead);
        s3.putObject(por);
        
        
	}
	
	
	public static void publishNotificationWithMessage (String message, Contact c) {
		
		String nextToken = null;
		Topic topicToPost = null;
		
		
		do {
			
			ListTopicsResult topicsResult = sns.listTopics();
			List<Topic> topics = topicsResult.getTopics();
			for (Topic t : topics) {
				if (t.getTopicArn().equals(TOPIC_ARN)) { 
					topicToPost = t;
					break;
				}
			}
			
		} while (nextToken != null);
		
		
		if (topicToPost != null) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("Contact name: " + c.getFirstName() + " " + c.getLastName());
			sb.append("\nvisit link at: https://s3.amazonaws.com/cloudhw1/" + c.filename + ".html");
			sns.publish(new PublishRequest(TOPIC_ARN, sb.toString(), message));
			
		}
		else System.out.println("failed to post notification");
		
	}
	

}
